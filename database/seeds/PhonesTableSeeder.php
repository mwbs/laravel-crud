<?php

use Illuminate\Database\Seeder;

class PhonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
	     * Create phones registers
	     *
	     */
        factory(App\Phone::class, 15)->create();
    }
}
