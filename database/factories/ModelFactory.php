<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' 		=> $faker->name,
        'email' 	=> $faker->unique()->safeEmail,
        'birth' 	=> $faker->date,
        'address' 	=> $faker->streetAddress,
        'cep' 		=> rand(10000, 99999) .'-'. rand(000, 999),
        'city' 		=> $faker->citySuffix,
        'state' 	=> $faker->state,
    ];
});


$factory->define(App\Phone::class, function (Faker\Generator $faker) {
    return [
        'number' => '('.rand(11, 99).')'. rand(10000000, 99999999),
        'user_id' => rand(1, 5),
    ];
});
