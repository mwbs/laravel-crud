 
@extends('layout')

@section('title', 'Novo usuário')

@section('content')

	<div class="row">
		<div class="container-fluid">
			<h3>Novo usuário</h3>
			<p>Informe os dados do novo usuário</p>
		</div>	
	</div>
	<div class="row">
		<div class="col-sm-12">
			<br>
			
			<div class="row">
				<div class="col-sm-12">
					@include('partials.message_validation')
				</div>				
			</div>


			<div class="well">

				<form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
					
					<!-- input user -->
					@include('user.form_user')
					
					<div class="btn-group" role="group">
						<a href="{{ route('users') }}" class="btn btn-danger">Voltar</a>
						<button type="submit" class="btn btn-primary right">Salvar</button>
					</div>
				</form>						
			</div>
			<br>

		</div>
	</div>
@endsection