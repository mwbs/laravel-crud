<div class="row">
	{{ csrf_field() }}						

	<div class="col-md-12">
		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
			<label for="name">Nome:</label>
			<input type="text" class="form-control" id="name" name="name" value="{{ isset($user->name) ? $user->name : '' }}" maxlength="254">
		</div>
	</div>
	<div class="col-md-9">
		<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
			<label for="email">Email:</label>
			<input type="email" class="form-control" id="email" name="email"  value="{{ isset($user->email) ? $user->email : '' }}" maxlength="254">
		</div>
	</div>
	<div class="col-md-3">	
		<div class="form-group {{ $errors->has('birth') ? 'has-error' : '' }}">
			<label for="birth">Data de nascimento:</label>
			<input type="date" class="form-control" id="birth" name="birth" value="{{ isset($user->birth) ? $user->birth : '' }}" maxlength="10">
		</div>
	</div>	
	<div class="col-md-12">
		<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
			<label for="address">Endereço:</label>
			<input type="text" class="form-control" id="address" name="address"  value="{{ isset($user->address) ? $user->address : '' }}" maxlength="254">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group {{ $errors->has('cep') ? 'has-error' : '' }}">
			<label for="cep">CEP:</label>
			<input type="text" class="form-control" id="cep" name="cep" value="{{ isset($user->cep) ? $user->cep : '' }}" maxlength="9">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
			<label for="city">Cidade:</label>
			<input type="text" class="form-control" id="city" name="city" value="{{ isset($user->city) ? $user->city : '' }}" maxlength="254">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
			<label for="state">Estado:</label>
			<input type="text" class="form-control" id="state" name="state" value="{{ isset($user->state) ? $user->state : '' }}" maxlength="254">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-10">
		<h4 class="title-form">Telefones</h4>
	</div>
	<div class="col-md-2">
		<button class="btn btn-info btn-newPhone" type="button" title="Novo telefone">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"> </span>
			Novo Telefone
		</button>
	</div>
</div>
<!-- List all phones -->
<div class="row section-phones">
	@if( empty($phones) )
		<div class="col-md-4">
			<div class="form-group">
				<label for="number">Telefone:</label>
				<input type="text" class="form-control" id="number" name="number[]">
			</div>
		</div>

	@else	
		@foreach($phones AS $phone)
			@if ($loop->first)
				<!-- First input is required -->
				<div class="col-md-4">
					<div class="form-group">
						<label for="number">Telefone: <small>(Obrigatório)</small></label>
						<input type="text" class="form-control" id="number" name="number[]" required value="{{ $phone->number }}">
					</div>
				</div>

			@else
				<div class="col-md-4">
					<div class="form-group">
						<label for="number">Telefone:</label>
						<input type="text" class="form-control" id="number" name="number[]" value="{{ $phone->number }}">
					</div>
				</div>

			@endif							
		@endforeach

	@endif
</div>