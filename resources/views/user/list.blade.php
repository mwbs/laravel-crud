@extends('layout')

@section('title', 'Usuarios')

@section('content')

<div class="row">
	<div class="container-fluid">
		<h3>Usuários</h3>
		<p>Área destinada para os usuários cadastrados no sistema</p>
	</div>	
</div>

<div class="row">	
	<div class="col-sm-3">	
		<a href="{{ route('user.create') }}" class="btn btn-primary text-center">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"> </span>
			Novo usuário
		</a>
	</div>
	
</div>
<div class="row">
	<div class="col-sm-12">
		<br>
		<div class="well">

			<table class="table table-hover">
				<thead>
					<tr>
						<th>Opções</th>
						<th width="25%">Nome</th>
						<th>Email</th>
						<th>Endereço</th>
						<th>Total telefones</th>
					</tr>
				</thead>
				<tbody>
					@forelse($users AS $user)
						<tr>
							<td>
								<a class="btn btn-success" href="{{ route('user.edit', $user->id) }}" role="button">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span>
								</a>
								<a class="btn btn-danger" href="{{ route('user.delete', $user->id) }}" role="button">
									<span class="glyphicon glyphicon-trash" aria-hidden="true"> </span>
								</a>
							</td>
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $user->address }}</td>
							<td class="text-center">
								{{ $user->total }}
							</td>
						</tr>
						@empty
							<tr>
								<td colspan="5">
									<p class="text-center">Não há usuários cadastrados</p>
								</td>
							</tr>

					@endforelse
				</tbody>
			</table>

		</div>
	</div>
</div>
@endsection