 
@extends('layout')

@section('title', 'Editar usuário')

@section('content')

	<div class="row">
		<div class="container-fluid">
			<h3>Edição do usuário</h3>
			<p>Atualização dos dados de {{ $user->name }}</p>
		</div>	
	</div>
	<div class="row">
		<div class="col-sm-12">
			<br>

			<div class="row">
				<div class="col-sm-12">
					@include('partials.message_validation')
				</div>				
			</div>

			<div class="well">

				<form action="{{ route('user.update') }}" method="post" enctype="multipart/form-data">
					
					<!-- input user -->
					@include('user.form_user')

					<input type="hidden" name="id" value="{{ $user->id }}">
					<div class="btn-group" role="group">
						<a href="{{ route('users') }}" class="btn btn-danger">Voltar</a>
						<button type="submit" class="btn btn-primary">Atualizar</button>
					</div>

					
				</form>						
			</div>
			<br>

		</div>
	</div>
@endsection