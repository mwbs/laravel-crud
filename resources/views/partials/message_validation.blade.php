
@if( session('errors') )
	<div class="alert alert-danger"> 
		<ul>
			@foreach($errors->all() AS $message)
				<li>{{ $message }}</li>
			@endforeach
		</ul>
	</div>
@endif