<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>CRUD | @yield('title')</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
	@php
		/* URL check */
		$url = explode(".", Route::currentRouteName());
		if ($url[0] == 'users' || $url[0] == 'user'){
			$url[0] = 'users'; 
		}
	@endphp

	<div class="container-fluid">
		<div class="row content">
			<div class="col-md-2 col-sm-12 sidenav">
				<h2 class="text-center">Laravel</h2>
				<ul class="nav nav-pills nav-stacked">
					<li class="{{ $url[0] == 'home' ? 'active' : '' }}">
						<a href="{{ route('home') }}">Home</a>
					</li>
					<li class="{{ $url[0] == 'users' ? 'active' : ''  }}">
						<a href="{{ route('users') }}">Usuários</a>
					</li>		
				</ul>
				<br>				
			</div>

			<div class="col-md-10 col-sm-12">
				<!-- Messages -->
				@include('partials.message')
		
				@yield('content')


			</div> 
			
		</div> <!-- Fim .row content-->
	</div> <!-- Fim .container-fluid-->
	
	


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script src="{{ asset('js/functions.js') }}"></script>

  </body>
</html>