<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\User;
use App\Phone;
use Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * @return [type]
     */
    public function index()
    {
        /* Select all users and count phones */
        $registers   = DB::table('users')
                    ->select( DB::raw('users.*, count(phones.id) AS total') )
                    ->leftJoin('phones', 'users.id', '=', 'phones.user_id')
                    ->groupBy('users.id')
                    ->orderBy('users.name', 'asc')
                    ->get();
    	return view('user.list', ['users' => $registers]);
    }


    /**
     * @return [type]
     */
    public function create()
    {
    	return view('user.create');
    }

    /**
     * @param  Request
     * @return [type]
     */
    public function store(Request $request)
    {
        /* Validation request users */
        $validator = Validator::make($request->all(), [
            'name'      => 'required|max:255',
            'email'     => 'required|max:255',
            'birth'     => 'required|date',
            'cep'       => 'required|max:9',
            'address'   => 'required|max:255',
            'city'      => 'required|max:255',
            'state'     => 'required|max:255',
        ]);

        if ($validator->fails()) {
            session()->flash('errors', $validator->errors());
            return redirect()->back();
        }

        $user = new User;
        $user->name     = $request->input('name'); 
        $user->email    = $request->input('email'); 
        $user->birth    = $request->input('birth'); 
        $user->cep      = $request->input('cep'); 
        $user->address  = $request->input('address'); 
        $user->city     = $request->input('city');
        $user->state    = $request->input('state'); 
        if( $user->save() ){
            $id = $user->id;
            $numbers = $request->input('number');

            foreach( $numbers as $n ) {

                /* Save if value of array do not empty */
                if ( !empty($n) ) {
                    $phone = new Phone;
                    $phone->number  = $n; 
                    $phone->user_id = $id; 
                    $phone->save();
                }
            }
        }
        else{
            session()->flash('type', 'danger');
            session()->flash('message', 'Erro ao cadastrar o usuário!');
            return redirect()->back();
        }

        session()->flash('type', 'success');
        session()->flash('message', 'Usuário cadastrado com sucesso!');
        return redirect()->route('users');
    }


    /**
     * @param  [type]
     * @return [type]
     */
    public function edit($id)
    {
        $user   = User::find($id);
        $phones = DB::table('phones')->where('user_id', $id)->get();
        return view('user.edit', compact('user'), ['phones' => $phones]);
    }



    public function update(Request $request)
    {   
        /* Validation request users */
        $validator = Validator::make($request->all(), [
            'name'      => 'required|max:255',
            'email'     => 'required|max:255',
            'birth'     => 'required|date',
            'cep'       => 'required|max:9',
            'address'   => 'required|max:255',
            'city'      => 'required|max:255',
            'state'     => 'required|max:255',
        ]);

        if ($validator->fails()) {
            session()->flash('errors', $validator->errors());
            return redirect()->back();
        }

        /* Update user */
        $id = $request->input('id');
        if( empty($id) ){
            session()->flash('type', 'danger');
            session()->flash('message', 'Identificador não encontrado!');
            return redirect()->back();
        }

        $user = User::find($id);
        $user->name     = $request->input('name'); 
        $user->email    = $request->input('email'); 
        $user->birth    = $request->input('birth'); 
        $user->cep      = $request->input('cep'); 
        $user->address  = $request->input('address'); 
        $user->city     = $request->input('city');
        $user->state    = $request->input('state'); 
        if( $user->save() ){
            /* Delete all phones */
            DB::table('phones')->where('user_id', $id)->delete();

            $numbers = $request->input('number');

            /* Save new phones */
            foreach( $numbers as $n ) {
                /* Save if value of array do not empty */
                if ( !empty($n) ) {
                    $phone = new Phone;
                    $phone->number  = $n; 
                    $phone->user_id = $id; 
                    $phone->save();
                }

            }

        }
        else{
            session()->flash('type', 'danger');
            session()->flash('message', 'Erro ao atualizar o usuário!');
            return redirect()->back();
        }

        session()->flash('type', 'success');
        session()->flash('message', 'Usuário atualizado com sucesso!');
        return redirect()->route('users');
    }



    public function delete($id)
    {
        $user = User::find($id);
        if( $user->delete() ){
            session()->flash('type', 'success');
            session()->flash('message', 'Usuário deletado com sucesso!');
            return redirect()->route('users');

        }
        else{
            session()->flash('type', 'danger');
            session()->flash('message', 'Erro ao deletar o usuário!');
            return redirect()->back();

        }
        
    }
}
