<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Home Routes */
Route::get('/', 
	['as'=>'home', 'uses'=>'Admin\HomeController@index']);

/* Users Routes */
Route::get('/users', 
	['as'=>'users', 'uses'=>'Admin\UserController@index']);

Route::get('/users/create', 
	['as'=>'user.create', 'uses'=>'Admin\UserController@create']);
Route::post('/users/store', 
	['as'=>'user.store', 'uses'=>'Admin\UserController@store']);

Route::get('/users/edit/{id}', 
	['as'=>'user.edit', 'uses'=>'Admin\UserController@edit']);
Route::post('/users/update', 
	['as'=>'user.update', 'uses'=>'Admin\UserController@update']);

Route::get('/users/delete/{id}', 
	['as'=>'user.delete', 'uses'=>'Admin\UserController@delete']);


