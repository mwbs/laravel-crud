 #Painel Laravel 

O painel laravel é um protótipo de um painel de controle, projetado para o gerenciamento de módulos de um site.

###Tecnologias
- Laravel 5.4
- Bootstrap 3
- Jquery 3.2.1
- CSS3
- HTML5

###Requisitos
- PHP >= 5.6.4 
- Laravel >= 5.4.*
- Laravel/tinker ~1.0

------------

##Manual de utilização
O painel possui um módulo de usuários, nele é possivel cadastrar, editar, visualizar e deletar usuários do sistema, cada usuário poderá ser vinculado n telefones. 

Autor: Marcos W. Brito
[Linkedin](https://www.linkedin.com/in/marcos-brito-webdev/ "Meu linkedin")

